import {NgModule} from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import {MaterialComponentsModule} from '../common/material-components/material-components.module';
import {TableViewComponent} from '../common/components/table-view/table-view.component';
import {FormsModule} from '@angular/forms';
import {UserDashComponent} from './user-dash/user-dash.component';
import {RouterModule} from '@angular/router';
import {RenewLicenseComponent} from './renew-license/renew-license.component';
import {ApplicatonStatusComponent} from './applicaton-status/applicaton-status.component';
import {ApplicatonVerificationComponent} from './applicaton-verification/applicaton-verification.component';
import {NewLicenseComponent} from './new-license/new-license.component';
import {BlockdataComponent} from './blockdata/blockdata.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TxnComponent} from './txn/txn.component';
import {AllApplicationsComponent} from './all-applications/all-applications.component';
import {DearComponent} from './dear/dear.component';
import {VerifyComponent} from './verify/verify.component';
import {TraDashComponent} from './tra-dash/tra-dash.component';
import {ChartsModule} from 'ng2-charts';
import {DafzaDashComponent} from './dafza-dash/dafza-dash.component';


@NgModule({
  declarations: [
    TableViewComponent,
    UserDashComponent,
    RenewLicenseComponent,
    ApplicatonStatusComponent,
    ApplicatonVerificationComponent,
    NewLicenseComponent,
    BlockdataComponent,
    TxnComponent,
    AllApplicationsComponent,
    DearComponent,
    VerifyComponent,
    TraDashComponent,
    DafzaDashComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ChartsModule,
    MaterialComponentsModule,
    NgxSpinnerModule
  ],
  providers: [CurrencyPipe]

})
export class PagesModule {
}
