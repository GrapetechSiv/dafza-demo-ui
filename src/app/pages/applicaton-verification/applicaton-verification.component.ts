import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ConfirmDialogueComponent} from '../../common/components/confirm-dialogue/confirm-dialogue.component';
import * as flat from 'flat';
import {ActivatedRoute} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-applicaton-verification',
  templateUrl: './applicaton-verification.component.html',
  styleUrls: ['./applicaton-verification.component.scss']
})
export class ApplicatonVerificationComponent implements OnInit {

  customHeaders: any = {
    thead: ['License Number', 'Company Name', 'Legal Name', 'Expiry Date', 'Status', 'Created'],
    displayed: ['license.licenseNum', 'companyName', 'legal.name', 'license.expiryDate', 'stat', 'created_at']
  };

  buttons: any = null;

  dataSource = [];
  application = null;
  status: String;
  view = null;
  action;

  type = null;

  message = 'Adding to Blockchian';

  statusMap = {
    'LICENSE_NEW_INITIATED': 'License Initiated',
    'PAYMENT_SUCCESS': 'Payment Finished',
    'NOC': 'Noc Issued',
    'LICENSE_ISSUE': 'Trade License Issued'
  };

  constructor(private apiService: ApiService,
              public dialog: MatDialog,
              public route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private snackBar: MatSnackBar) {

  }

  ngOnInit() {

    // this.spinner.hide();
    this.route.params.subscribe(params => {
      const type = params['type']; // (+) converts string 'id' to a number
      this.dataSource = [];
      this.application = null;
      switch (type) {
        case 'noc':
          this.type = 'NOC';
          this.buttons = {
            thead: ['VIEW'],
            btns: [
              {action: 'view', icon: 'find_in_page', text: 'View Application', type: 'btn', status: 'NOC'}
            ]
          };
          this.dataFetchDAFZA();
          break;
        case 'certificate':
          this.type = 'Certificate';
          this.buttons = {
            thead: ['VIEW'],
            btns: [
              {action: 'view', icon: 'find_in_page', text: 'View Application', type: 'btn', status: 'LICENSE_ISSUE'}
            ]
          };

          this.getAllApplicationsForCertIssue();
          break;

      }
    });


  }

  actions($event: any) {

    console.log('actions', $event);

    let dialogueRef = null;

    switch ($event.status) {

      case 'LICENSE_ISSUE':
        dialogueRef = this.dialog.open(ConfirmDialogueComponent, {
          data: {
            postitveBtn: 'Issue Trade License',
            data: this.findTheDocument($event._id),
            action: $event.status
          }
        });

        break;

      case 'NOC':
        dialogueRef = this.dialog.open(ConfirmDialogueComponent, {
          data: {
            postitveBtn: 'Request for NOC',
            data: this.findTheDocument($event._id),
            action: $event.status
          }
        });
        break;
    }

    if (dialogueRef) {
      dialogueRef.afterClosed().subscribe(result => {

        console.log('[action]', result);

        try {


          if ($event.status === result.action) {

            this.spinner.show();
            this.apiService.updateApplicationWithStatus($event._id, result.action, result.remark).subscribe(data => {
              // this.spinner.hide();

              if (result.action === 'NOC') {

                setTimeout(() => {
                  this.message = 'Requesting for NOC';
                }, 500);

                setTimeout(() => {
                  this.message = 'Calling TRA smart contract.';
                }, 2000);
                setTimeout(() => {
                  this.message = 'Creating NOC';
                }, 3000);

                setTimeout(() => {
                  this.message = 'Adding data to Blockchain.';
                  this.spinner.hide();
                  console.log('[updateApplicationWithStatus]', 'Adding data to Blockchain');
                }, 4000);

                this.snackBar.open('NOC Issued', null, {
                  duration: 2000,
                });

                this.ngOnInit();

              } else {
                this.spinner.hide();

                this.snackBar.open('Certificate Issued', null, {
                  duration: 2000,
                });

                this.ngOnInit();
              }


              console.log('[updateApplicationWithStatus]', 'Overr here');

            });
          } else if (result.action === 'rejected') {
            this.apiService.updateApplicationWithStatus($event._id, 'rejected', result.remark).subscribe(data => {
              this.ngOnInit();
            });
          }

        } catch (e) {

        }
      });

    }


  }


  dataFetchDAFZA() {
    this.dataSource = [];
    this.application = [];

    this.apiService.getAllApplicationsbystatus('PAYMENT_SUCCESS').subscribe(data => {
      if (data && data['success']) {
        console.log(data['data']);
        this.application = data['data'];
        data['data'].forEach((d) => {
          d.stat = this.statusMap[d.status[d.status.length - 1]];
          d.license.expiryDate = new Date(d.license.expiryDate).toLocaleString();
          d.created_at = new Date(d.created_at).toLocaleString();
          this.dataSource.push(flat(d));
        });

        console.log(this.dataSource);
      }
    });
  }

  private getAllApplicationsForCertIssue() {
    this.dataSource = [];
    this.application = [];
    console.log('[getAllApplicationsForCertIssue]', this.dataSource, this.application);

    this.apiService.getAllApplicationsbystatus('NOC').subscribe(data => {
      if (data && data['success']) {
        console.log(data['data']);
        this.application = data['data'];
        const temp = [];
        data['data'].forEach((d) => {
          d.stat = this.statusMap[d.status[d.status.length - 1]];
          d.license.expiryDate = new Date(d.license.expiryDate).toDateString();
          d.created_at = new Date(d.created_at).toLocaleString();
          temp.push(flat(d));
        });
        this.dataSource = temp;
        console.log(this.dataSource);
      }
    });
  }

  viewDetails(row) {
    console.log(row);
  }

  private findTheDocument(_id: any) {
    let data = null;
    if (this.application && this.application.length > 0) {
      for (let i = 0; i < this.application.length; i++) {
        console.log(i);
        if (this.application[i]._id === _id) {
          data = this.application[i];
          break;
        }
      }
    }
    console.log('[findTheDocument]', data);
    return data;
  }
}
