import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicatonVerificationComponent } from './applicaton-verification.component';

describe('ApplicatonVerificationComponent', () => {
  let component: ApplicatonVerificationComponent;
  let fixture: ComponentFixture<ApplicatonVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicatonVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicatonVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
