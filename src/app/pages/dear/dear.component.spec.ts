import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DearComponent} from './dear.component';

describe('DearComponent', () => {
  let component: DearComponent;
  let fixture: ComponentFixture<DearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DearComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
