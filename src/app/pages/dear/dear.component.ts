import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-dear',
  templateUrl: './dear.component.html',
  styleUrls: ['./dear.component.scss']
})
export class DearComponent implements OnInit {

  id = null;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      if (params['id']) {
        this.id = params['id'];
      } else {
        this.router.navigate(['/app/status']);
      }
      // (+) converts string 'id' to a number

    });

  }

}
