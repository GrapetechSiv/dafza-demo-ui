import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TraDashComponent} from './tra-dash.component';

describe('TraDashComponent', () => {
  let component: TraDashComponent;
  let fixture: ComponentFixture<TraDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TraDashComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
