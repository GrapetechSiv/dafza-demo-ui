import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-blockdata',
  templateUrl: './blockdata.component.html',
  styleUrls: ['./blockdata.component.scss']
})
export class BlockdataComponent implements OnInit {

  blocks = null;
  currentBlock = null;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {

    this.apiService.getAllBlocks().subscribe((blocks) => {

      // console.log(blocks);

      if (blocks && blocks['data']) {

        this.blocks = blocks['data'];
      }

    });
  }

  parse(value: any) {
    return JSON.parse(value);
  }
}
