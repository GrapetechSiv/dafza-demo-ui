import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ConfirmDialogueComponent} from '../../common/components/confirm-dialogue/confirm-dialogue.component';
import {MatDialog} from '@angular/material';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-txn',
  templateUrl: './txn.component.html',
  styleUrls: ['./txn.component.scss']
})
export class TxnComponent implements OnInit {

  customHeaders: any = {
    thead: ['TxId', 'Amount', 'License NO', 'From', 'To', 'Credit/Debit', 'Date'],
    displayed: ['_id', 'amount', 'liscenceno', 'from', 'to', 'ts', 'created_at']

  };

  buttons = {
    thead: ['VIEW'],
    btns: [
      {action: 'view', icon: 'find_in_page', text: 'View Application', type: 'btn', status: 'VIEW'}
    ]
  };

  application = null;
  txns = [];

  role = localStorage.getItem('role');

  constructor(private apiService: ApiService, private dialog: MatDialog, private cp: CurrencyPipe) {
  }

  ngOnInit() {
    this.apiService.getAllTransactions().subscribe(data => {

      if (data['data'] && data['data'].length > 0) {
        this.application = data['data'];
        console.log(this.application);
        const rows = [];
        data['data'].forEach((txn) => {
          txn.created_at = new Date(txn.created_at).toLocaleString();
          txn.amount = this.cp.transform(txn.amount, 'AED ');
          txn.from = (txn.from).toUpperCase();
          txn.to = (txn.to).toUpperCase();

          if (this.role === 'tra') {
            txn.ts = 'Credit';
          }

          rows.push(txn);
        });

        this.txns = rows;

      }
    });
  }

  actions($event) {
    console.log('actions', $event);

    let dialogueRef = null;

    switch ($event.status) {

      case 'VIEW':
        dialogueRef = this.dialog.open(ConfirmDialogueComponent, {
          data: {
            data: this.findTheDocument($event._id),
            action: $event.status,
            hide: 'hide'
          }
        });

        break;

    }
  }

  private findTheDocument(_id: any) {
    let data = null;
    if (this.application && this.application.length > 0) {
      for (let i = 0; i < this.application.length; i++) {
        console.log(i);
        if (this.application[i]._id === _id) {
          data = this.application[i]['data'];
          break;
        }
      }
    }
    console.log('[findTheDocument]', data);
    return data;
  }


}
