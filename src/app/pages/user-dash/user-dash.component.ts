import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import * as flat from 'flat';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-dash',
  templateUrl: './user-dash.component.html',
  styleUrls: ['./user-dash.component.scss']
})
export class UserDashComponent implements OnInit {

  application = null;

  licenceHeaders: any = {
    thead: ['License Number', 'Expiry Date', 'Status', 'Created', 'Action'],
    displayed: ['license.licenseNum', 'license.expiryDate', 'status', 'created_at', 'action']
  };

  licencedata = [];

  user = null;

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit() {

    this.apiService.getAllApplications().subscribe(application => {

      if (application && application['success']) {
        this.application = application['data'][0];

        const temp = [];
        application['data'].forEach((app) => {
          // app.license.firstExpDate = new Date(app.license.firstExpDate).toDateString();
          app.license.expiryDate = new Date(app.license.expiryDate).toDateString();
          app.license.licenseNum = 'L' + app._id.substring(0, 8).toUpperCase();
          app.created_at = new Date(app.created_at).toLocaleString();

          if (app.status.includes('LICENSE_ISSUE')) {
            app = flat(app);
            app.status = 'Trade License Issued';
            app.action = {action: 'status', icon: 'visibility', text: 'View Application Status', type: 'btn', status: 'STATUS'};
          } else {
            app = flat(app);
            app.action = {action: 'renew', icon: 'visibility', text: 'Renew Application', type: 'btn', status: 'RENEW'};
            app.status = 'Under Processing';
          }

          temp.push(app);
          console.log(this.licencedata);
        });
        this.licencedata = temp;
      }
    });


    this.apiService.getUser().subscribe(data => {
      if (data) {
        this.user = data['data']['data'];
      }
    });
  }

  getCounter(number: number) {
    return Array(5).fill({date: new Date()});
  }

  selectRow($event: any) {

    // console.log($event);

  }

  action($event: any) {

    if ($event.action === 'renew') {
      this.router.navigate(['/app/status', $event._id]);
    } else if ($event.action === 'status') {
      this.router.navigate(['/app/status', $event._id]);
    }
  }
}
