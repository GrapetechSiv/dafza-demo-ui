import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicatonStatusComponent } from './applicaton-status.component';

describe('ApplicatonStatusComponent', () => {
  let component: ApplicatonStatusComponent;
  let fixture: ComponentFixture<ApplicatonStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicatonStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicatonStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
