import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute} from '@angular/router';
import * as html2canvas from 'html2canvas';
import QRCode from 'qrcode';

@Component({
  selector: 'app-applicaton-status',
  templateUrl: './applicaton-status.component.html',
  styleUrls: ['./applicaton-status.component.scss']
})
export class ApplicatonStatusComponent implements OnInit {

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
  }

  leaseHeaders: any = {
    thead: ['Lease Number', 'Office Number', 'Term Start Date', 'Term Expiry Date', 'Status'],
    displayed: ['leaseNum', 'officeNum', 'startDate', 'expiryDate', 'leaseStatus']
  };
  activityHeaders: any = {
    thead: ['Activity Id', 'Activity Group', 'Activity Name', 'NOC Issuer', 'NOC Fee'],
    displayed: ['id', 'group', 'Name', 'nocIssuer', 'fee']
  };
  application = null;
  applications = null;
  date = Date.now();

  qr64 = null;


  static convertBase64ToBlobData(b64Data: any, fileame) {
    fetch(b64Data)
      .then((res) => {

        res.blob().then(b => {
          console.log(b);
          if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE
            window.navigator.msSaveOrOpenBlob(b, fileame);
          } else { // chrome
            const url = window.URL.createObjectURL(b);
            // window.open(url);
            const link = document.createElement('a');
            link.href = url;
            link.download = fileame;
            link.click();
          }
        });

      });
  }


  ngOnInit() {

    this.apiService.getAllApplications().subscribe(applications => {
      if (applications && applications['success']) {
        this.applications = applications['data'];
        this.route.params.subscribe(params => {
          const id = params['id']; // (+) converts string 'id' to a number
          if (id) {
            applications['data'].forEach((application) => {
              if (application._id === id) {
                this.qr64 = this.qr(application._id);
                this.application = application;
              }
            });
          }
        });
      }
    });


  }


  download(id, licenseNo) {

    const certificate = (<HTMLInputElement>document.getElementById(id));

    html2canvas(certificate, {
      useCORS: true,
      allowTaint: true,
      letterRendering: true,
      scale: 1,
    }).then(canvas => {

      const ctx = canvas.getContext('2d');
      ctx.webkitImageSmoothingEnabled = false;
      ctx.mozImageSmoothingEnabled = false;
      ctx.imageSmoothingEnabled = false;

      ApplicatonStatusComponent.convertBase64ToBlobData(canvas.toDataURL('image/png'), licenseNo + id + Date.now() + '.png');

      // console.log(canvas.toDataURL('image/png'));
    });


  }


  async qr(data) {
    try {
      const q = await QRCode.toDataURL(data);
      this.qr64 = q;
    } catch (err) {
      console.error(err);
    }
  }

  setQr(_id: any) {
    this.qr(_id);
  }
}
