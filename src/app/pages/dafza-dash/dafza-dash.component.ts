import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-dafza-dash',
  templateUrl: './dafza-dash.component.html',
  styleUrls: ['./dafza-dash.component.scss']
})
export class DafzaDashComponent implements OnInit {

  customHeaders: any = {
    thead: ['TxId', 'Amount', 'License NO', 'From', 'To', 'Credit/Debit', 'Date'],
    displayed: ['_id', 'amount', 'liscenceno', 'from', 'to', 'ts', 'created_at']

  };

  txns = [];

  dashData = null;
  // lineChart
  public lineChartData: Array<any> = [
    {data: [], label: 'DAFZA', fill: false},
    {data: [], label: 'FZA', fill: false}
  ];

  public lineChartLabels: any = [];

  public lineChartType = 'line';
  public pieChartType = 'doughnut';

  public lineChartOptions: any = {
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        }
      }]
    }
  };

  // Pie
  public pieChartLabels: string[] = ['TRA', 'DAFZA'];
  public pieChartData: number[] = [300, 500];
  colors: any = [
    { // grey
      backgroundColor: ['orange', 'rgba(9, 62, 82, .8)'],
      borderColor: ['orange', 'rgba(9, 62, 82, .1)'],
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  constructor(private apiService: ApiService, private cp: CurrencyPipe) {
    this.getMetaData();
  }

  public randomizeType(): void {
    // this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
    // this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomizePie(): void {
    // this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
    this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }

  ngOnInit() {

    this.apiService.getAllTransactions().subscribe(data => {

      if (data['data'] && data['data'].length > 0) {
        const rows = [];
        data['data'].forEach((txn) => {
          txn.created_at = new Date(txn.created_at).toLocaleString();
          txn.amount = this.cp.transform(txn.amount, 'AED ');
          txn.from = (txn.from).toUpperCase();
          txn.to = (txn.to).toUpperCase();
          rows.push(txn);
        });

        this.txns = rows;

      }
    });

  }


  getMetaData() {
    this.apiService.getTRAMeta().subscribe(res => {
      if (res && res['success']) {
        this.dashData = res['data'];
        this.setNOCCharts('day');
        this.setAmountChart();
      }
    });
  }


  setNOCCharts(type) {
    const tempLabel = new Set<any>();
    this.lineChartData[0]['data'] = [];
    const monthNameList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    if (type === 'month') {
      monthNameList.forEach((month, index) => {
        tempLabel.add(monthNameList[index]);
        this.lineChartData[0]['data'].push(0);
      });
    }
    this.dashData.allApplication.forEach((app) => {

      //
      if (type === 'month' && app._id[type]) {
        monthNameList.forEach((month, index) => {
          if (index === app._id[type] - 1) {
            this.lineChartData[0]['data'][index] = (app.count);
          }
        });

      } else {
        tempLabel.add(app._id[type]);
        this.lineChartData[0]['data'].push(app.count);
      }

      console.log(type, app._id, app._id[type]);
    });

    this.lineChartLabels = Array.from(tempLabel);

  }

  setAmountChart() {
    this.pieChartData[0] = this.dashData.amountToDafzaTRA[0].total;
    this.pieChartData[1] = this.dashData.amountToDafzaTRA[1].total;
  }

}
