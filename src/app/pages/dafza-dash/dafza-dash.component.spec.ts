import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DafzaDashComponent} from './dafza-dash.component';

describe('DafzaDashComponent', () => {
  let component: DafzaDashComponent;
  let fixture: ComponentFixture<DafzaDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DafzaDashComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DafzaDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
