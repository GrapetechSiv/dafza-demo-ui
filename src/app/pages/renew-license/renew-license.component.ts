import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-renew-license',
  templateUrl: './renew-license.component.html',
  styleUrls: ['./renew-license.component.scss']
})
export class RenewLicenseComponent implements OnInit {

  leaseHeaders: any = {
    thead: ['Lease Number', 'Office Number', 'Term Start Date', 'Term Expiry Date', 'Status'],
    displayed: ['leaseNum', 'officeNum', 'startDate', 'expiryDate', 'leaseStatus']
  };
  activityHeaders: any = {
    thead: ['Activity Id', 'Activity Group', 'Activity Name', 'NOC Issuer', 'NOC Fee'],
    displayed: ['id', 'group', 'Name', 'nocIssuer', 'fee']
  };


  leasedata = [];
  activityData = [];
  isLinear = true;

  formData = {
    _id: null,
    terms: 1,
    files: {
      dcaaNOC: null,
      lsc: null,
      wcip: null,
      fapip: null,
      thirdParty: null,
      ctl: null,
      osm: null
    },
  };

  application = null;
  newExpiryDate = null;

  constructor(private apiService: ApiService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      const id = params['id']; // (+) converts string 'id' to a number
      this.apiService.getAllApplications().subscribe(applications => {
        console.log('yes');
        if (applications && applications['success']) {

          applications['data'].forEach((application) => {

            if (application._id === id) {

              this.application = application;
              this.leasedata = this.application.Lease;
              this.activityData = this.application.activity;
              this.formData._id = this.application._id;
            }

          });

        }
      });
    });

  }

  uploadAndset(meta: string, model: any, event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {

      const file: File = fileList[0];

      console.log('file', file);

      const formData: FormData = new FormData();
      formData.append('filename', file, file.name);


      this.apiService.uploadFile(formData).subscribe((data) => {
        if (data && data['success']) {
          console.log(data);
          this.formData.files[meta] = data['url'];
          console.log(this.formData);
        }
      });

    } else {
      this.formData.files[meta] = null;
      console.warn('Error no files');
    }

  }

  submitRenewal() {
    this.apiService.postApplications(this.formData).subscribe(res => {
      if (res && res['success']) {
        this.router.navigate(['/app/status', this.formData._id]);
      }
    });
  }

  newExpiry(firstExpDate: any) {
    const newExpiry = new Date(firstExpDate);
    newExpiry.setFullYear(newExpiry.getFullYear() + parseInt(this.formData.terms + '', 10));
    this.newExpiryDate = newExpiry.toISOString();
  }
}
