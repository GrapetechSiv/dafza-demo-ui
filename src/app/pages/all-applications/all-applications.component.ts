import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ApiService} from '../../services/api.service';
import * as flat from 'flat';
import {MatDialog} from '@angular/material';
import {ConfirmDialogueComponent} from '../../common/components/confirm-dialogue/confirm-dialogue.component';

@Component({
  selector: 'app-all-applications',
  templateUrl: './all-applications.component.html',
  styleUrls: ['./all-applications.component.scss']
})
export class AllApplicationsComponent implements OnInit {

  role = null;

  customHeaders: any = {
    thead: ['License Number', 'Company Name', 'Legal Name', 'Expiry Date', 'Status', 'Created'],
    displayed: ['license.licenseNum', 'companyName', 'legal.name', 'license.expiryDate', 'stat', 'created_at'],

  };
  application = null;
  buttons = {
    thead: ['VIEW'],
    btns: [
      {action: 'view', icon: 'find_in_page', text: 'View Application', type: 'btn', status: 'VIEW'}
    ]
  };

  dataSource = [];

  status = {
    'LICENSE_NEW_INITIATED': 'License Initiated',
    'PAYMENT_SUCCESS': 'Payment Finished',
    'NOC': 'Noc Issued',
    'LICENSE_ISSUE': 'Trade License Issued'
  };

  constructor(private route: ActivatedRoute, private apiService: ApiService, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.role = localStorage.getItem('role');
    if (this.role === 'dafza') {
      this.fetchAllDAZFA();
    } else if (this.role === 'fza') {
      this.fetchAllFZA();
    } else {

      this.customHeaders = {
        thead: ['License Number', 'Company Name', 'Legal Name', 'Expiry Date', 'Status', 'Free Zone'],
        displayed: ['license.licenseNum', 'companyName', 'legal.name', 'license.expiryDate', 'stat', 'privateFor'],

      };
      this.fetchAllTRA();
    }
  }


  actions($event) {
    console.log('actions', $event);

    let dialogueRef = null;

    switch ($event.status) {

      case 'VIEW':
        dialogueRef = this.dialog.open(ConfirmDialogueComponent, {
          data: {
            data: this.findTheDocument($event._id),
            action: $event.status,
            hide: 'hide'
          }
        });

        break;

    }
  }


  private findTheDocument(_id: any) {
    let data = null;
    if (this.application && this.application.length > 0) {
      for (let i = 0; i < this.application.length; i++) {
        console.log(i);
        if (this.application[i]._id === _id) {
          data = this.application[i];
          break;
        }
      }
    }
    console.log('[findTheDocument]', data);
    return data;
  }

  private fetchAllTRA() {
    this.application = [];
    this.dataSource = [];
    this.apiService.getAllApplications().subscribe(data => {
      if (data && data['success']) {
        this.application = data['data'];
        const temp = [];
        data['data'].forEach((d) => {
          d.stat = this.status[d.status[d.status.length - 1]];
          d['license'].expiryDate = new Date(d.license.expiryDate).toLocaleString();
          d.created_at = new Date(d.created_at).toLocaleString();
          console.log(d.stat, d.status[d.status.length - 1]);
          d.privateFor = (d.privateFor).toUpperCase();
          temp.push(flat(d));
        });
        this.dataSource = temp;
      }
    });
  }


  private fetchAllDAZFA() {
    this.application = [];
    this.dataSource = [];
    this.apiService.getAllApplicationsForDafzaOnly().subscribe(data => {
      if (data && data['success']) {
        this.application = data['data'];
        const temp = [];
        data['data'].forEach((d) => {
          d.stat = this.status[d.status[d.status.length - 1]];
          d['license'].expiryDate = new Date(d.license.expiryDate).toLocaleString();
          d.created_at = new Date(d.created_at).toLocaleString();

          temp.push(flat(d));
        });
        this.dataSource = temp;
      }
    });
  }

  private fetchAllFZA() {
    this.application = [];
    this.dataSource = [];
    this.apiService.getAllApplicationsForFZAOnly().subscribe(data => {
      if (data && data['success']) {
        this.application = data['data'];
        data['data'].forEach((d) => {
          d.stat = this.status[d.status[d.status.length - 1]];
          d['license'].expiryDate = new Date(d.license.expiryDate).toLocaleString();
          d.created_at = new Date(d.created_at).toLocaleString();
          this.dataSource.push(flat(d));
        });
      }
    });
  }
}
