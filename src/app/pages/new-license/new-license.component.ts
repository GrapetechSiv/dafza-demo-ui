import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-license',
  templateUrl: './new-license.component.html',
  styleUrls: ['./new-license.component.scss']
})
export class NewLicenseComponent implements OnInit {

  leaseHeaders: any = {
    thead: ['Lease Number', 'Office Number', 'Term Start Date', 'Term Expiry Date', 'Status'],
    displayed: ['leaseNum', 'officeNum', 'startDate', 'expiryDate', 'leaseStatus']
  };
  activityHeaders: any = {
    thead: ['Activity Id', 'Activity Group', 'Activity Name', 'NOC Issuer', 'NOC Fee'],
    displayed: ['id', 'group', 'Name', 'nocIssuer', 'fee']
  };

  isLinear = true;
  paymentOption = 'Credit Card';

  formData = {
    type: null,
    officeNum: null,
    startDate: null,
    leaseTerm: 1,
    activity: [],
    leasedata: [],
    insurance: false,
    files: {
      lsc: null,
      wcip: null,
      fapip: null,
      thirdParty: null,
      osm: null
    },
    amount: 0
  };


  constructor(private apiService: ApiService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {


  }

  uploadAndset(meta: string, model: any, event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {

      const file: File = fileList[0];

      console.log('file', file);

      const formData: FormData = new FormData();
      formData.append('filename', file, file.name);


      this.apiService.uploadFile(formData).subscribe((data) => {
        if (data && data['success']) {
          console.log(data);
          this.formData.files[meta] = data['url'];
          console.log(this.formData);
        }
      });

    } else {
      this.formData.files[meta] = null;
      console.warn('Error no files');
    }

  }

  submitApplication() {
    this.formData.amount = (this.formData.leaseTerm * 10000) + 5300;
    this.apiService.createApplications(this.formData).subscribe(res => {
      if (res && res['success']) {
        this.router.navigate(['/app/dear-customer', res['data']._id]);
        // this.router.navigate(['/app/status', res['data']._id]);
      }
    });
  }


  addLease(n, o, s, e) {
    this.formData.leasedata.push({
      _id: Date.now(),
      leaseNum: n.value,
      officeNum: o.value,
      startDate: s.value,
      expiryDate: e.value,
      leaseStatus: 'Processing'
    });

    n.value = null;
    o.value = null;
    s.value = null;
    e.value = null;


  }

  deleteLease($event) {

  }

  addActivity(f, g, h) {


    this.formData.activity.push({
      _id: Date.now(),
      id: Date.now().toString().substr(Date.now().toString().length / 2 + 3, 4),
      group: f.value,
      Name: g.value,
      nocIssuer: h.value,
      fee: '200'
    });

    f.value = null;
    g.value = null;
    h.value = null;
  }
}

