import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLicenseComponent } from './new-license.component';

describe('NewLicenseComponent', () => {
  let component: NewLicenseComponent;
  let fixture: ComponentFixture<NewLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
