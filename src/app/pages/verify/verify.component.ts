import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  view = null;

  constructor(private apiService: ApiService, private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  searchByID(applicationId) {
    this.spinner.show();
    this.view = null;
    this.apiService.getApplicationById(applicationId).subscribe((data) => {
      if (data && data['success']) {
        this.view = data['data'];
        this.spinner.hide();
      }
    });

  }
}
