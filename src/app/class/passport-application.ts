export class PassportApplication {

  constructor(
    public emiratesId: string,
    public policeStation: string,
    public lostPlace: string,
    public email: string,
    public lostDate: string,
    public comments: string,
    public mobile: number,
    public minor: boolean,
    public document: string,
    public type: string

  ) {
  }
}
