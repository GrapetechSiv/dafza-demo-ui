import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {MaterialComponentsModule} from './common/material-components/material-components.module';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PagesModule} from './pages/pages.module';
import {FormsModule} from '@angular/forms';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';
import {ConfirmDialogueComponent} from './common/components/confirm-dialogue/confirm-dialogue.component';
import {ChartsModule} from 'ng2-charts';


@NgModule({
    declarations: [
        AppComponent,
        ConfirmDialogueComponent,
        LoginComponent,
        DashboardComponent,
    ],
    entryComponents: [ConfirmDialogueComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
      ChartsModule,
        MaterialComponentsModule,
        PagesModule
    ],
    providers: [ApiService, {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
