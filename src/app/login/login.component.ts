import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string = null;
  password: string = null;
  logo = null;
  isUser = false;

  constructor(private apiService: ApiService, private router: Router, private titleService: Title) {

    this.isUser = (this.router.url === '/login');
    this.logo = (this.router.url === '/login') ? 'user' : this.router.url.substring(1);
    this.titleService.setTitle(this.logo.toUpperCase());
  }

  ngOnInit() {



  }


  login() {

    if (this.username && this.password) {

      this.apiService.login({username: this.username, password: this.password}).subscribe((res) => {
        if (res && res['success']) {
          localStorage.setItem('token', res['token']);
          localStorage.setItem('role', res['role']);
          this.router.navigate(['/app']);
        }
      });
    }
  }

}
