import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent implements OnInit , OnChanges{

  @Input() dataSource: any;
  @Input() headers: any;
  @Input() buttons: any;

  @Input() update: boolean;
  @Input() delete: boolean;

  @Input() shadow = true;
  @Input() pagination = true;
  @Input() indexing = true;
  @Input() perpage = 10;
  @Input() paginationSelect = true;

  @Output() deleteRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() updateRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectRow: EventEmitter<any> = new EventEmitter<any>();

  @Output() buttonAction: EventEmitter<any> = new EventEmitter<any>();

  data: any = {};

  displayed = [];

  total: number;
  page = 1;

  checked: any = [];
  checkAll = false;
  dialogue = false;
  dialogeData: any = {};

  constructor() {
  }

  ngOnInit() {
    this.processData();
  }

  ngOnChanges() {
    this.processData();
  }

  selected(e, id) {
    if (e.target.checked) {
      this.checked.push(id);
    }
  }

  deleteSelected() {
    if (this.checked.length > 0) {
      this.deleteRow.emit(this.checked);
      this.dataSource = this.dataSource.filter((value, index, array) => {
        return !this.checked.includes(value.id);
      });
      this.checked = [];
      this.paginate(this.page);
    }
  }

  totalItems() {
    if (this.total > this.perpage) {
      return Math.ceil(this.total / this.perpage);
    } else {
      return 1;
    }
  }

  paginate(page) {
    const start = (this.perpage * page) - this.perpage;
    const end = (this.perpage * page);
    // console.log(start, end);
    this.data['data'] = this.dataSource.slice(start, end);
    // console.log(this.data);
  }

  pageOnChange(perPage) {
    this.perpage = perPage;
    this.page = 1;
    // console.log(perPage);
    this.paginate(this.page);
  }

  previousPage() {
    if (this.page > 1) {
      this.page--;
    }

    this.paginate(this.page);
  }

  nextPage() {
    if (this.page + 1 <= this.totalItems()) {
      this.page++;
    }

    this.paginate(this.page);
  }

  showUpdate(id: any) {
    this.dialogue = true;
    // console.log('Showing the updates');
    this.data.data.forEach((value) => {
      if (value.id === id) {
        this.dialogeData = value;
      }
    });

  }

  closeDialogue() {
    this.dialogue ? this.dialogue = false : this.dialogue = true;
    this.dialogeData = {};
  }

  submitUpdateRow(dialogeData: any) {
    this.updateRow.emit(dialogeData);
  }

  private processData() {


    if (this.buttons && this.buttons.length > 0) {

    }

    if (this.dataSource && this.dataSource.length > 0) {
      if (this.headers.thead && this.headers.thead.length > 0) {
        this.data['headers'] = this.headers.thead;
        this.displayed = this.headers.displayed;
      } else {
        console.warn('No headers data for table provided');
      }

      this.total = this.dataSource.length;
      this.paginate(this.page);
    } else {
      console.warn('No data for table provided');
      this.data['data'] = this.dataSource;
    }
  }

  fireRowClick(row) {
    if (row) {
      this.selectRow.emit(row);
    }
  }

  sendAction(action, id, status) {

    this.buttonAction.emit({action: action, _id: id, status: status});
    console.log('[sendAction]', action , id);
  }

  typeOf(rowElement: any) {
    return (typeof rowElement === 'object') ? (( Array.isArray(rowElement)) ? 'array' : 'object') : typeof rowElement ;
  }
}
