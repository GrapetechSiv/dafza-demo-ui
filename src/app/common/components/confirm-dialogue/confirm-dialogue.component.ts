import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import QRCode from 'qrcode';

export interface DialogData {
  action: string;
  header: string;
  message: string;
}

@Component({
  selector: 'app-confirm-dialogue',
  templateUrl: './confirm-dialogue.component.html',
  styleUrls: ['./confirm-dialogue.component.scss']
})

export class ConfirmDialogueComponent implements OnInit {
  qr64 = null;
  role = localStorage.getItem('role');

  dateNow = Date.now();

  status = {
    'LICENSE_NEW_INITIATED': 'License Initiated',
    'PAYMENT_SUCCESS': 'Payment Finished',
    'NOC': 'Noc Issued',
    'LICENSE_ISSUE': 'Trade License Issued'
  };

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }

  ngOnInit() {
    this.setQr(this.data['data']._id);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  isImage(url) {
    return url.substr((url.lastIndexOf('.') + 1));
  }

  async qr(data) {
    try {
      const q = await QRCode.toDataURL(data);
      this.qr64 = q;
    } catch (err) {
      console.error(err);
    }
  }

  setQr(_id: any) {
    this.qr(_id);
  }


}
