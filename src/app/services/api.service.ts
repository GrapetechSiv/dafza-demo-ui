import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class ApiService {

  baseURL = environment.server;


  constructor(private http: HttpClient) {


  }

  login(body: any) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.baseURL + '/login', body, headers);
  }

  verify() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.baseURL + '/verify', {token: localStorage.getItem('token')}, headers);
  }

  getAllApplications() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplications', {}, headers);
  }

  uploadFile(formData: FormData) {
    const formHeader = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data;',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/upload', formData);
  }

  postApplications(body) {

    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/postApplications', body, headers);
  }

  createApplications(body) {

    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/createNewApplications', body, headers);
  }

  updateApplicationWithStatus(id, status, remark) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };

    return this.http.post(this.baseURL + '/updateStatus',
      {_id: id, status: status, remark: remark}, headers);
  }

  getIssueApplications() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplicationsForDafza', {}, headers);
  }

  getAllApplicationsForDafzaVerified() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplicationsForDafzaVerified', {}, headers);
  }

  getAllApplicationsbystatus(status) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplicationsbystatus', {status: status}, headers);
  }

  getUser() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getUser', {}, headers);
  }

  getAllBlocks() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getBlocks', {}, headers);
  }

  getAllTransactions() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllTransactions', {}, headers);
  }

  getAllApplicationsForDafzaOnly() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplicationsForDafza', {}, headers);
  }

  getAllApplicationsForFZAOnly() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getAllApplicationsForfza', {}, headers);
  }

  getApplicationById(applicationId: any) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/getApplicationById', {_id: applicationId}, headers);
  }

  getTRAMeta() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-token': localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseURL + '/nocData', {}, headers);
  }
}
