import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {Router} from '@angular/router';

import {animate, group, query, style, transition, trigger} from '@angular/animations';

export const routerTransition = trigger('routerTransition', [
  transition('* <=> *', [
    /* order */
    /* 1 */ query(':enter, :leave', style({position: 'fixed', width: '100%'})
      , {optional: true}),
    /* 2 */ group([  // block executes in parallel
      query(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('0.5s linear', style({transform: 'translateY(0%)'}))
      ], {optional: true}),
      query(':leave', [
        style({transform: 'translateX(0%)'}),
        animate('0.5s linear', style({transform: 'translateX(-100%)'}))
      ], {optional: true}),
    ])
  ])
]);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  animations: [routerTransition],
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private apiService: ApiService, private router: Router) {
  }


  navigationData = {
    leftPanelMenuDetails: []
  };
  menuDisplayName: any = 'Dubai Police';
  width: any = null;
  role: string;


  ngOnInit() {

    this.width = window.innerWidth;

    const role = localStorage.getItem('role');
    this.role = role;

    if (!role) {
      this.router.navigate(['/login']);
    }

    switch (role) {
      case 'user':
        this.navigationData = {
          leftPanelMenuDetails: [
            {menuDisplayName: 'Dashboard', action: 'dashboard', menuDisplayIcon: 'dashboard'},
            // {menuDisplayName: 'Profile', action: 'dashboard', menuDisplayIcon: 'business'},
            {menuDisplayName: 'Status', action: 'status', menuDisplayIcon: 'receipt'},
            // {menuDisplayName: 'Services', action: 'dashboard', menuDisplayIcon: 'supervised_user_circle'},

          ]
        };
        this.redirect();
        break;

      case 'tra':
        this.navigationData = {
          leftPanelMenuDetails: [
            {menuDisplayName: 'Dashboard', action: 'tra-dashboard', menuDisplayIcon: 'dashboard'},
            {menuDisplayName: 'All Applications', action: 'all-applications', menuDisplayIcon: 'receipt'},
            {menuDisplayName: 'Bank Transactions', action: 'bank-txn', menuDisplayIcon: 'dashboard'},
            {menuDisplayName: 'Block View', action: 'block-view', menuDisplayIcon: 'dashboard'},
          ]
        };
        this.redirect();
        break;

      case 'fza':
        this.navigationData = {
          leftPanelMenuDetails: [
            {menuDisplayName: 'All Applications', action: 'all-applications', menuDisplayIcon: 'receipt'},
          ]
        };
        this.redirect();

        break;
      case 'dafza':

        this.navigationData = {
          leftPanelMenuDetails: [
            {menuDisplayName: 'Dashboard', action: 'dafza-dashboard', menuDisplayIcon: 'dashboard'},
            {menuDisplayName: 'NOC Issue', action: 'application-verification/noc', menuDisplayIcon: 'receipt'},
            {menuDisplayName: 'Certificate Issue', action: 'application-verification/certificate', menuDisplayIcon: 'receipt'},
            {menuDisplayName: 'Bank Transactions', action: 'bank-txn', menuDisplayIcon: 'dashboard'},
            {menuDisplayName: 'Block View', action: 'block-view', menuDisplayIcon: 'dashboard'},
          ]
        };
        this.redirect();
        break;

    }

  }


  redirect() {
    const redirect = this.navigationData['leftPanelMenuDetails'][0];

    if (this.router.url === '/app') {
      if (redirect) {
        this.router.navigate(['/app/' + redirect.action]);
      }
    } else {
      // this.router.navigate(['/app/' + redirect.action]);
    }
  }


  getState(outlet) {
    return outlet.activatedRouteData.state;
  }

}
