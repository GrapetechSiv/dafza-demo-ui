import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {LoginComponent} from '../login/login.component';
import {UserDashComponent} from '../pages/user-dash/user-dash.component';
import {RenewLicenseComponent} from '../pages/renew-license/renew-license.component';
import {ApplicatonStatusComponent} from '../pages/applicaton-status/applicaton-status.component';
import {ApplicatonVerificationComponent} from '../pages/applicaton-verification/applicaton-verification.component';
import {NewLicenseComponent} from '../pages/new-license/new-license.component';
import {BlockdataComponent} from '../pages/blockdata/blockdata.component';
import {TxnComponent} from '../pages/txn/txn.component';
import {AllApplicationsComponent} from '../pages/all-applications/all-applications.component';
import {DearComponent} from '../pages/dear/dear.component';
import {VerifyComponent} from '../pages/verify/verify.component';
import {TraDashComponent} from '../pages/tra-dash/tra-dash.component';
import {DafzaDashComponent} from '../pages/dafza-dash/dafza-dash.component';


const routes: Routes = [

  {path: 'login', component: LoginComponent},
  {path: 'tra', component: LoginComponent},
  {path: 'dafza', component: LoginComponent},
  {path: 'fza', component: LoginComponent},
  {
    path: 'app',
    component: DashboardComponent,
    canActivate: [],
    children: [
      {path: 'dashboard', component: UserDashComponent},
      {path: 'tra-dashboard', component: TraDashComponent},
      {path: 'dafza-dashboard', component: DafzaDashComponent},
      {path: 'renew/:id', component: RenewLicenseComponent},
      {path: 'status/:id', component: ApplicatonStatusComponent},
      {path: 'status', component: ApplicatonStatusComponent},
      {path: 'new-license', component: NewLicenseComponent},
      {path: 'application-verification/:type', component: ApplicatonVerificationComponent},
      {path: 'block-view', component: BlockdataComponent},
      {path: 'bank-txn', component: TxnComponent},
      {path: 'all-applications', component: AllApplicationsComponent},
      {path: 'dear-customer/:id', component: DearComponent},
      {path: 'verify', component: VerifyComponent},

    ]
  },
  {path: '', redirectTo: '/login', pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: false})
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
}
